# TP1 : correction
Copie partielle du dépôt principal (https://bitbucket.org/olevitt/formations/).  
Pour récupérer la correction du TP1, vous pouvez au choix :  
* Cloner ce dépôt (via git clone, URL disponible dans le bouton clone en haut à droite)  
* Télécharger le zip du dépôt via l'onglet Downloads  
  

Une fois ce dépôt récupéré (et éventuellement dézippé), vous pouvez importer le projet sous eclipse via file, import, General, Existing projects into workspace.