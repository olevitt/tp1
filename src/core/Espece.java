package core;

public class Espece {
	
	public int numero;
	public String nom;
	public String type;
	public int pvBase;
	public int forceBase;
	
	public String toString() {
		return "Espece [numero=" + numero + ", nom=" + nom + ", type=" + type + ", pvBase=" + pvBase + ", forceBase="
				+ forceBase + "]";
	}
	
	

}
